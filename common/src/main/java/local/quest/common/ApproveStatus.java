package local.quest.common;

public enum  ApproveStatus {

    not_exist,
    not_checked,
    approved,
    not_approved

}
