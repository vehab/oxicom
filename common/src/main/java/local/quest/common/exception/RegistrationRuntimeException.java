package local.quest.common.exception;

public class RegistrationRuntimeException
        extends RuntimeException {

    public RegistrationRuntimeException(String message) {
        this(message, null);
    }

    public RegistrationRuntimeException(String message,
                                        Throwable cause) {
        super(message, cause);
    }

}
