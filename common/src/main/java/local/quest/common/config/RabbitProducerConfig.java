package local.quest.common.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitProducerConfig {

    // RabbitTemplate -> RabbitOperations -> AmqpTemplate
    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory factory,
                                         ObjectMapper objectMapper) {
        Jackson2JsonMessageConverter producerConverter = new Jackson2JsonMessageConverter(objectMapper);

        RabbitTemplate rabbitTemplate = new RabbitTemplate(factory);
        rabbitTemplate.setMessageConverter(producerConverter);
        return rabbitTemplate;
    }

}
