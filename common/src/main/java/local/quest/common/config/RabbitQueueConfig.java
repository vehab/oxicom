package local.quest.common.config;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitQueueConfig {

    public static final String EXCHANGE_FANOUT_MAILER_AND_API = "exchange-fanout-mailer-an-api";
    public static final String QUEUE_API_SERVICE = "api_queue";
    public static final String QUEUE_APPROVER_SERVICE = "approver_queue";
    public static final String QUEUE_MAILER_SERVICE = "mailer_queue";

    @Bean
    public AmqpAdmin amqpAdmin(ConnectionFactory factory) {
        return new RabbitAdmin(factory);
    }

    @Bean
    public Queue queueApiService() {
        return new Queue(QUEUE_API_SERVICE, false);
    }

    @Bean
    public Queue queueApproveService() {
        return new Queue(QUEUE_APPROVER_SERVICE, false);
    }

    @Bean
    public Queue queueMailerService() {
        return new Queue(QUEUE_MAILER_SERVICE, false);
    }

    @Bean
    public FanoutExchange exchangeFanoutForMailerAndApi() {
        return new FanoutExchange(EXCHANGE_FANOUT_MAILER_AND_API);
    }

    @Bean
    public Binding bindingApi() {
        return BindingBuilder.bind(queueApiService()).to(exchangeFanoutForMailerAndApi());
    }

    @Bean
    public Binding bindingMailer() {
        return BindingBuilder.bind(queueMailerService()).to(exchangeFanoutForMailerAndApi());
    }

}
