package local.quest.common.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import local.quest.common.City;
import local.quest.common.model.dto.UserInfoDto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@JsonDeserialize(as = UserInfoDto.class)
public interface UserInfo
        extends Serializable {

    String getFirstName();

    String getLastName();

    String getEmail();

    LocalDate getBirthday();

    City getCityOfResidence();

    City getCityOfRegistration();

    static UserInfo of(String firstName,
                       String lastName,
                       String email,
                       LocalDate birthday,
                       City cityOfResidence,
                       City cityOfRegistration) {
        Objects.requireNonNull(firstName);
        Objects.requireNonNull(lastName);
        Objects.requireNonNull(email);
        Objects.requireNonNull(birthday);
        Objects.requireNonNull(cityOfResidence);
        Objects.requireNonNull(cityOfRegistration);

        return new UserInfo() {
            @Override
            public String getFirstName() {
                return firstName;
            }

            @Override
            public String getLastName() {
                return lastName;
            }

            @Override
            public String getEmail() {
                return email;
            }

            @Override
            public LocalDate getBirthday() {
                return birthday;
            }

            @Override
            public City getCityOfResidence() {
                return cityOfResidence;
            }

            @Override
            public City getCityOfRegistration() {
                return cityOfRegistration;
            }

            @Override
            public String toString() {
                return String.format("UserInfo{firstName='%1$s', lastName='%2$s', email='%3$s', birthday=%4$s, cityOfResidence=%5$s, cityOfRegistration=%6$s}",
                        firstName, lastName, email, birthday, cityOfResidence.name(), cityOfRegistration.name());
            }
        };
    }

}
