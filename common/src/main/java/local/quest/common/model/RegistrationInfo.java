package local.quest.common.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import local.quest.common.ApproveStatus;
import local.quest.common.model.dto.RegistrationInfoDto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@JsonDeserialize(as = RegistrationInfoDto.class)
public interface RegistrationInfo<I>
        extends Serializable {

    I getId();

    LocalDateTime getRegistrationDateTime();

    UserInfo getUserInfo();

    ApproveStatus getApproveStatus();

    LocalDateTime getApproveDateTime();

    static <I> RegistrationInfo<I> of(I id,
                                      LocalDateTime registrationDateTime,
                                      UserInfo userInfo,
                                      ApproveStatus approveStatus,
                                      LocalDateTime approveDateTime) {
        Objects.requireNonNull(id);
        Objects.requireNonNull(registrationDateTime);
        Objects.requireNonNull(userInfo);
        Objects.requireNonNull(approveStatus);
        Objects.requireNonNull(approveDateTime);

        return new RegistrationInfo<I>() {
            @Override
            public I getId() {
                return id;
            }

            @Override
            public LocalDateTime getRegistrationDateTime() {
                return registrationDateTime;
            }

            @Override
            public UserInfo getUserInfo() {
                return userInfo;
            }

            @Override
            public ApproveStatus getApproveStatus() {
                return approveStatus;
            }

            @Override
            public LocalDateTime getApproveDateTime() {
                return approveDateTime;
            }

            @Override
            public String toString() {
                return String.format("RegistrationInfo{id='%1$s', registrationDateTime='%2$s', userInfo=%3$s, approveStatus=%4$s, approveDateTime='%5$s'}",
                        id, registrationDateTime, userInfo, approveStatus.name(), approveDateTime);
            }
        };
    }

}
