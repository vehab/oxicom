package local.quest.common.model.dto;

import local.quest.common.City;
import local.quest.common.model.UserInfo;

import java.time.LocalDate;

public class UserInfoDto
        implements UserInfo {

    private String firstName;
    private String lastName;
    private String email;
    private LocalDate birthday;
    private City cityOfResidence;
    private City cityOfRegistration;

    public UserInfoDto() {
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    @Override
    public City getCityOfResidence() {
        return cityOfResidence;
    }

    public void setCityOfResidence(City cityOfResidence) {
        this.cityOfResidence = cityOfResidence;
    }

    @Override
    public City getCityOfRegistration() {
        return cityOfRegistration;
    }

    public void setCityOfRegistration(City cityOfRegistration) {
        this.cityOfRegistration = cityOfRegistration;
    }

    @Override
    public String toString() {
        return String.format("UserInfoDto{firstName='%1$s', lastName='%2$s', email='%3$s', birthday=%4$s, cityOfResidence=%5$s, cityOfRegistration=%6$s}",
                firstName, lastName, email, birthday, cityOfResidence.name(), cityOfRegistration.name());
    }

}
