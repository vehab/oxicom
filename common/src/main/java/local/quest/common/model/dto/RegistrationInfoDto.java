package local.quest.common.model.dto;

import local.quest.common.ApproveStatus;
import local.quest.common.model.RegistrationInfo;
import local.quest.common.model.UserInfo;

import java.time.LocalDateTime;

public class RegistrationInfoDto<I>
        implements RegistrationInfo<I> {

    private I id;
    private LocalDateTime registrationDateTime;
    private UserInfo userInfo;
    private ApproveStatus approveStatus;
    private LocalDateTime approveDateTime;

    public RegistrationInfoDto() {
    }

    @Override
    public I getId() {
        return id;
    }

    public void setId(I id) {
        this.id = id;
    }

    @Override
    public LocalDateTime getRegistrationDateTime() {
        return registrationDateTime;
    }

    public void setRegistrationDateTime(LocalDateTime registrationDateTime) {
        this.registrationDateTime = registrationDateTime;
    }

    @Override
    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    @Override
    public ApproveStatus getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(ApproveStatus approveStatus) {
        this.approveStatus = approveStatus;
    }

    @Override
    public LocalDateTime getApproveDateTime() {
        return approveDateTime;
    }

    public void setApproveDateTime(LocalDateTime approveDateTime) {
        this.approveDateTime = approveDateTime;
    }

    @Override
    public String toString() {
        return String.format("RegistrationInfoDto{id='%1$s', registrationDateTime='%2$s', userInfo=%3$s, approveStatus=%4$s, approveDateTime='%5$s'}",
                id, registrationDateTime, userInfo, approveStatus.name(), approveDateTime);
    }

}
