package local.quest.approver.service;

import local.quest.common.ApproveStatus;
import local.quest.common.config.RabbitQueueConfig;
import local.quest.common.model.RegistrationInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import java.lang.invoke.MethodHandles;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Service
public class ApproverServiceImpl
        implements ApproverService<String> {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private static final String RN = System.lineSeparator();

    private final RabbitTemplate rabbitTemplate;

    public ApproverServiceImpl(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = Objects.requireNonNull(rabbitTemplate);
    }

    @Override
    @RabbitListener(queues = RabbitQueueConfig.QUEUE_APPROVER_SERVICE)
    public void validateRegistration(RegistrationInfo<String> registrationInfo) {
        sleepMillis(3_500);

        Period period = Period.between(registrationInfo.getUserInfo().getBirthday(), LocalDate.now());
        ApproveStatus approveStatus = period.getYears() > 18 ? ApproveStatus.approved : ApproveStatus.not_approved;
        RegistrationInfo validatedRegistrationInfo =
                RegistrationInfo.of(
                        registrationInfo.getId(),
                        registrationInfo.getRegistrationDateTime(),
                        registrationInfo.getUserInfo(),
                        approveStatus,
                        LocalDateTime.now());
        log.info("approver : validate registration :{}- period={},{}- registration_info={},{}- registration_info={}",
                RN, period, RN, registrationInfo, RN, validatedRegistrationInfo);
        rabbitTemplate.setExchange(RabbitQueueConfig.EXCHANGE_FANOUT_MAILER_AND_API);
        rabbitTemplate.convertAndSend(validatedRegistrationInfo);
    }

    private void sleepMillis(int millis) {
        try {
            TimeUnit.MILLISECONDS.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
