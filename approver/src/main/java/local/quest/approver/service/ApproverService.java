package local.quest.approver.service;

import local.quest.common.model.RegistrationInfo;

public interface ApproverService<I> {

    void validateRegistration(RegistrationInfo<I> registrationInfo);

}
