package local.quest.approver;

import local.quest.common.config.JacksonConfig;
import local.quest.common.config.RabbitConsumerConfig;
import local.quest.common.config.RabbitProducerConfig;
import local.quest.common.config.RabbitQueueConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(
        scanBasePackages = "local.quest.approver",
        scanBasePackageClasses = {
                JacksonConfig.class,
                RabbitQueueConfig.class, RabbitConsumerConfig.class, RabbitProducerConfig.class})
public class ApproverServiceApplication {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(ApproverServiceApplication.class);
        app.run(args);
    }

}
