
Система регистрации пользователей с отправкой email после одобрения 

### rabbitmq
http://localhost:15672/
guest + guest

### service
http://localhost:8080/api/v1/test
http://localhost:8081/id-gen/v1/generate/string/36

### h2-console
http://localhost:8088

### doc

Listener container для асинхронной обработки входящих сообщений
RabbitTemplate для отправки и получения сообщений
RabbitAdmin для автоматического описания запросов, обмена и связывания

Для работы с RabbitMQ нам потребуются следующие бины:
- connectionFactory        - для соединения с RabbitMQ;
- rabbitAdmin              - для регистрации/отмены регистрации очередей и т.п.;
- rabbitTemplate           - для отправки сообщений (producer);
- myQueue1                 - собственно очередь куда посылаем сообщения;
- messageListenerContainer - принимает сообщения (consumer).

### need
- docker, docker-compose, docker+spring
- vue, axios
- h2 -> pg
- it-test, testcontainer
- sse

- consul
- hystrix (resilience4j)

- modules
- refactor
