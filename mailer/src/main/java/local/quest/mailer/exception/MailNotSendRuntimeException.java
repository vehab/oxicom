package local.quest.mailer.exception;

import local.quest.common.ApproveStatus;
import local.quest.common.exception.RegistrationRuntimeException;
import local.quest.common.model.UserInfo;

import java.util.Objects;

public class MailNotSendRuntimeException
        extends RegistrationRuntimeException {

    private final UserInfo userInfo;
    private final ApproveStatus approveStatus;

    public MailNotSendRuntimeException(UserInfo userInfo,
                                       ApproveStatus approveStatus,
                                       Throwable cause) {
        super(String.format("Mail not send! %1$s", userInfo), cause);

        this.userInfo = Objects.requireNonNull(userInfo);
        this.approveStatus = Objects.requireNonNull(approveStatus);
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public ApproveStatus getApproveStatus() {
        return approveStatus;
    }

    @Override
    public String toString() {
        return String.format("MailNotSendRuntimeException{userInfo=%1$s, approveStatus=%2$s}",
                userInfo, approveStatus.name());
    }

}
