package local.quest.mailer;

import local.quest.common.config.JacksonConfig;
import local.quest.common.config.RabbitConsumerConfig;
import local.quest.common.config.RabbitQueueConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(
        scanBasePackages = "local.quest.mailer",
        scanBasePackageClasses = {
                JacksonConfig.class,
                RabbitQueueConfig.class, RabbitConsumerConfig.class})
public class MailerServiceApplication {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(MailerServiceApplication.class);
        app.run(args);
    }

}
