package local.quest.mailer.service;

import local.quest.common.ApproveStatus;
import local.quest.common.config.RabbitQueueConfig;
import local.quest.common.model.RegistrationInfo;
import local.quest.mailer.exception.MailNotSendRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.lang.invoke.MethodHandles;
import java.util.Objects;

@Service
public class MailerServiceImpl
        implements MailerService<String> {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private final JavaMailSender emailSender;

    public MailerServiceImpl(JavaMailSender emailSender) {
        this.emailSender = Objects.requireNonNull(emailSender);
    }

    @Override
    @RabbitListener(queues = RabbitQueueConfig.QUEUE_MAILER_SERVICE)
    public void sendEmail(RegistrationInfo<String> registrationInfo) throws MailNotSendRuntimeException {
        try {
            log.info("mailer : send email : registration_info={}", registrationInfo);

            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(
                    //registrationInfo.getUserInfo().getEmail()
                    "spring_mail_sender_test_2020@mail.ru");
            message.setSubject("registration status : " + registrationInfo.getApproveStatus().name()
                    + " " + getSmile(registrationInfo.getApproveStatus()));
            message.setText("hello world : ");

            //emailSender.send(message);
        } catch (Exception e) {
            throw new MailNotSendRuntimeException(registrationInfo.getUserInfo(), registrationInfo.getApproveStatus(), e);
        }
    }

    private String getSmile(ApproveStatus approveStatus) {
        switch (approveStatus) {
            case approved:
                return ":)";
            case not_approved:
                return ":(";
            default:
                return "";
        }
    }

}
