package local.quest.mailer.service;

import local.quest.common.model.RegistrationInfo;
import local.quest.mailer.exception.MailNotSendRuntimeException;

public interface MailerService<I> {

    void sendEmail(RegistrationInfo<I> registrationInfo) throws MailNotSendRuntimeException;

}
