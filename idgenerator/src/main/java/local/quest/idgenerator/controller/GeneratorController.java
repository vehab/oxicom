package local.quest.idgenerator.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.invoke.MethodHandles;
import java.util.Objects;
import java.util.function.Supplier;

@RestController
@RequestMapping(path = "/id-gen/v1")
public class GeneratorController {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private final Supplier<String> generatorString36;

    public GeneratorController(Supplier<String> generatorString36) {
        this.generatorString36 = Objects.requireNonNull(generatorString36);
    }

    @GetMapping(path = "/generate/string/36")
    public String generateString36() {
        String val = generatorString36.get();
        log.info("generateString36 : {}", val);
        return val;
    }

}
