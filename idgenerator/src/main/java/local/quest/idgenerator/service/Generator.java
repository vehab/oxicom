package local.quest.idgenerator.service;

import java.security.SecureRandom;
import java.util.UUID;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public interface Generator<T>
        extends Supplier<T> {

    T generate();

    default T get() {
        return generate();
    }

    static Generator<Long> ofSystemMillis() {
        return System::currentTimeMillis;
    }

    static Generator<Long> ofSystemNanoTime() {
        return System::nanoTime;
    }

    static Generator<Integer> ofSystemNanoTimeToInt() {
        return () -> Math.abs(Long.valueOf(System.nanoTime()).intValue());
    }

    // length 36, with dash
    static Generator<String> ofUUIDx36() {
        return () -> UUID.randomUUID().toString();
    }

    // length 32, without dash
    static Generator<String> ofUUIDx32() {
        return () -> UUID.randomUUID().toString().replace("-", "");
    }

    // a-f0-9 (hex)
    static Generator<String> ofAf09(int n) {
        return () -> PrivateUtils.generateRandomString(PrivateUtils.CHAR_HEX, n);
    }

    // a-z0-9
    static Generator<String> ofAz09(int n) {
        return () -> PrivateUtils.generateRandomString(PrivateUtils.CHAR_LOWER, n);
    }

    class PrivateUtils {

        static final SecureRandom SECURE_RANDOM = new SecureRandom();

        static final String CHAR_NUMBER = "0123456789";
        static final String CHAR_HEX = CHAR_NUMBER + "abcdef";
        static final String CHAR_LOWER = "abcdefghijklmnopqrstuvwxyz";
        static final String CHAR_UPPER = CHAR_LOWER.toUpperCase();

        static String generateRandomString(String letters,
                                           int length) {
            if (length < 1) {
                throw new IllegalArgumentException();
            }
            int nn = letters.length();

            return IntStream.rangeClosed(1, length)
                    .map(i -> SECURE_RANDOM.nextInt(nn))
                    .mapToObj(letters::charAt)
                    .map(v -> Character.toString(v))
                    .collect(Collectors.joining());
        }

        private PrivateUtils() {
            // empty
        }

    }

}
