package local.quest.idgenerator;

import com.fasterxml.uuid.EthernetAddress;
import com.fasterxml.uuid.Generators;
import local.quest.idgenerator.service.Generator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import java.util.function.Supplier;

@SpringBootApplication
public class IdGeneratorApplication {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(IdGeneratorApplication.class);
        app.setWebApplicationType(WebApplicationType.SERVLET);
        app.run(args);
    }

    @Bean
    public Supplier<String> generatorString36ByJavaUuid() {
        return Generator.ofUUIDx36();
    }

    @Primary
    @Bean
    public Supplier<String> generatorString36ByEthernetAddress() {
        // Generators.randomBasedGenerator() vs Generators.timeBasedGenerator()
        return () -> Generators.timeBasedGenerator(EthernetAddress.fromInterface()).generate().toString();
    }

}
