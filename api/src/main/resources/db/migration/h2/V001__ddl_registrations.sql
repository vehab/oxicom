
--drop table if exists registrations;

create table registrations (
  id                     VARCHAR(36) PRIMARY KEY,
  registration_date_time TIMESTAMP(9) WITH TIME ZONE NOT NULL,
  first_name             VARCHAR(50) NOT NULL,
  last_name              VARCHAR(50) NOT NULL,
  email                  VARCHAR(64) NOT NULL,
  birthday               DATE NOT NULL,
  city_of_residence      VARCHAR(512),
  city_of_registration   VARCHAR(512),
  approve_status         VARCHAR(20) NOT NULL,
  approve_date_time      TIMESTAMP(9) WITH TIME ZONE NOT NULL
);

COMMENT ON TABLE  registrations                        IS 'регистрация пользователя';
COMMENT ON COLUMN registrations.registration_date_time IS 'дата и время регистрация';
COMMENT ON COLUMN registrations.first_name             IS 'имя пользователя';
COMMENT ON COLUMN registrations.last_name              IS 'фамилия пользователя';
COMMENT ON COLUMN registrations.email                  IS 'email пользователя';
COMMENT ON COLUMN registrations.birthday               IS 'день рождения';
COMMENT ON COLUMN registrations.city_of_residence      IS 'город проживания';
COMMENT ON COLUMN registrations.city_of_registration   IS 'город регистрации';
COMMENT ON COLUMN registrations.approve_status         IS 'approve status';
COMMENT ON COLUMN registrations.approve_date_time      IS 'дата и время approve';
