package local.quest.api.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.sql.SQLException;

@Profile("h2")
@Component
public class H2Console {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Value("${spring.h2.console.web-port}")
    private Integer h2ConsoleWebPort;
    @Value("${spring.h2.console.tcp-port}")
    private Integer h2ConsoleTcpPort;

    private org.h2.tools.Server webServer;
    private org.h2.tools.Server server;

    @EventListener(ContextRefreshedEvent.class)
    public void start() throws SQLException {
        log.info("stopping h2 console at port : web={}, tcp={}", h2ConsoleWebPort, h2ConsoleTcpPort);
        webServer = org.h2.tools.Server.createWebServer("-webPort", h2ConsoleWebPort.toString(), "-tcpAllowOthers").start();
        server = org.h2.tools.Server.createTcpServer("-tcpPort", h2ConsoleTcpPort.toString(), "-tcpAllowOthers").start();
    }

    @EventListener(ContextClosedEvent.class)
    public void stop() {
        log.info("stopping h2 console at port : web={}, tcp={}", h2ConsoleWebPort, h2ConsoleTcpPort);
        webServer.stop();
        server.stop();
    }

}
