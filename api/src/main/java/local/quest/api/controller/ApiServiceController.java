package local.quest.api.controller;

import local.quest.api.service.ApiService;
import local.quest.common.ApproveStatus;
import local.quest.common.City;
import local.quest.common.model.RegistrationInfo;
import local.quest.common.model.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.lang.invoke.MethodHandles;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Objects;
import java.util.Random;

/**
 * -- POST api/send-verification
 * в теле запроса - данные формы,
 * ответ - id-запроса в систему одобрения
 * <p>
 * -- GET api/check-verification-status
 * ответ - статус обращения (одобрено, не одобрено)
 */
@RestController
@RequestMapping(path = "/api/v1")
public class ApiServiceController {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private final ApiService<String> apiService;

    public ApiServiceController(ApiService<String> apiService) {
        this.apiService = Objects.requireNonNull(apiService);
    }

    @GetMapping(path = "/test")
    public Mono<String> test() {
        UserInfo userInfo =
                UserInfo.of("Jack", "London", "mark-twain@writer.book",
                        LocalDate.of(1965 + new Random().nextInt(50), Month.NOVEMBER, 30),
                        City.anapa, City.berlin);
        RegistrationInfo<String> registrationInfo =
                RegistrationInfo.of("test_id_" + System.currentTimeMillis(), LocalDateTime.now(), userInfo, ApproveStatus.not_checked,
                        LocalDateTime.now());
        log.info("test : registration_info={}", registrationInfo);
        return apiService.sendVerification(userInfo);
    }

    @GetMapping(path = "/check-verification/{reg_id}")
    public ApproveStatus checkVerificationStatus(@PathVariable(value = "reg_id") String registrationId) {
        log.info("checkVerificationStatus : reg_id='{}'", registrationId);
        return apiService.checkVerificationStatus(registrationId);
    }

    @PostMapping(path = "/send-verification")
    public Mono<String> sendVerification(@RequestBody UserInfo userInfo) {
        log.info("sendVerification : user_info={}", userInfo);
        return apiService.sendVerification(userInfo);
    }

}
