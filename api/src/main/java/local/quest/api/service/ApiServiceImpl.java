package local.quest.api.service;

import local.quest.api.db.ApiServiceRepository;
import local.quest.common.ApproveStatus;
import local.quest.common.config.RabbitQueueConfig;
import local.quest.common.exception.RegistrationRuntimeException;
import local.quest.common.model.RegistrationInfo;
import local.quest.common.model.UserInfo;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitOperations;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.Objects;

@Service
public class ApiServiceImpl
        implements ApiService<String> {

    private final TransactionOperations transactionOperations;
    private final WebClient webClientForIdGeneratorService;
    private final RabbitOperations rabbitOperations;
    private final ApiServiceRepository<String> repository;

    public ApiServiceImpl(TransactionOperations transactionOperations,
                          WebClient webClientForIdGeneratorService,
                          RabbitOperations rabbitOperations,
                          ApiServiceRepository<String> repository) {
        this.transactionOperations = Objects.requireNonNull(transactionOperations);
        this.webClientForIdGeneratorService = Objects.requireNonNull(webClientForIdGeneratorService);
        this.rabbitOperations = Objects.requireNonNull(rabbitOperations);
        this.repository = Objects.requireNonNull(repository);
    }

    @Override
    public Mono<String> sendVerification(UserInfo userInfo) {
        return webClientForIdGeneratorService.get()
                .uri("/generate/string/36")
                .retrieve()
                .bodyToMono(String.class)
                .map(registrationId ->
                        RegistrationInfo.of(registrationId, LocalDateTime.now(), userInfo, ApproveStatus.not_checked, LocalDateTime.now()))
                .doOnNext(registrationInfo ->
                        transactionOperations.executeWithoutResult(transactionStatus ->
                                repository.insertRegistrationInfo(registrationInfo)))
                .doOnNext(registrationInfo ->
                        // todo : to new thread
                        new Thread(() -> rabbitOperations.convertAndSend(RabbitQueueConfig.QUEUE_APPROVER_SERVICE, registrationInfo)).start())
                .map(RegistrationInfo::getId);
    }

    @Override
    public ApproveStatus checkVerificationStatus(String registrationId) {
        return transactionOperations.execute(transactionStatus ->
                repository.selectApproveStatus(registrationId)
                        .orElseThrow(() ->
                                new RegistrationRuntimeException("checkVerificationStatus : registration_id='" + registrationId + "'")));
    }

    @Override
    @RabbitListener(queues = RabbitQueueConfig.QUEUE_API_SERVICE)
    public void updateApproveStatus(RegistrationInfo<String> registrationInfo) {
        transactionOperations.executeWithoutResult(transactionStatus ->
                repository.updateApproveStatus(registrationInfo.getId(), registrationInfo.getApproveStatus(), registrationInfo.getApproveDateTime()));
    }

}
