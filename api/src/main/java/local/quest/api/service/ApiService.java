package local.quest.api.service;

import local.quest.common.ApproveStatus;
import local.quest.common.model.RegistrationInfo;
import local.quest.common.model.UserInfo;
import reactor.core.publisher.Mono;

public interface ApiService<I> {

    Mono<I> sendVerification(UserInfo userInfo);

    ApproveStatus checkVerificationStatus(I registrationId);

    void updateApproveStatus(RegistrationInfo<String> registrationInfo);

}
