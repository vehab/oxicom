package local.quest.api.db;

import local.quest.common.ApproveStatus;
import local.quest.common.City;
import local.quest.common.model.RegistrationInfo;
import local.quest.common.model.UserInfo;
import org.springframework.jdbc.core.namedparam.EmptySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
public class ApiServiceRepositoryImpl
        implements ApiServiceRepository<String> {

    private final NamedParameterJdbcOperations namedParameterJdbcOperations;
    private final Class<String> registrationIdClass;

    public ApiServiceRepositoryImpl(NamedParameterJdbcOperations namedParameterJdbcOperations) {
        this.namedParameterJdbcOperations = Objects.requireNonNull(namedParameterJdbcOperations);
        this.registrationIdClass = String.class;
    }

    @Override
    public void insertRegistrationInfo(RegistrationInfo<String> registrationInfo) {
        namedParameterJdbcOperations.update(
                "insert into registrations (id, registration_date_time, first_name, last_name, email, birthday, city_of_residence, city_of_registration, approve_status, approve_date_time)" +
                        " values (:id, :registration_date_time, :first_name, :last_name, :email, :birthday, :city_of_res, :city_of_reg, :approve_status, :approve_date_time)",
                new MapSqlParameterSource()
                        .addValue("id", registrationInfo.getId())
                        .addValue("registration_date_time", registrationInfo.getRegistrationDateTime())
                        .addValue("first_name", registrationInfo.getUserInfo().getFirstName())
                        .addValue("last_name", registrationInfo.getUserInfo().getLastName())
                        .addValue("email", registrationInfo.getUserInfo().getEmail())
                        .addValue("birthday", registrationInfo.getUserInfo().getBirthday())
                        .addValue("city_of_res", registrationInfo.getUserInfo().getCityOfResidence().name())
                        .addValue("city_of_reg", registrationInfo.getUserInfo().getCityOfRegistration().name())
                        .addValue("approve_status", registrationInfo.getApproveStatus().name())
                        .addValue("approve_date_time", registrationInfo.getApproveDateTime()));
    }

    @Override
    public void updateApproveStatus(String registrationId,
                                    ApproveStatus approveStatus,
                                    LocalDateTime aproveDateTime) {
        namedParameterJdbcOperations.update("" +
                        " update registrations" +
                        " set    approve_status    = :approve_status," +
                        "        approve_date_time = :approve_date_time" +
                        " where  id = :id",
                new MapSqlParameterSource()
                        .addValue("id", registrationId)
                        .addValue("approve_status", approveStatus.name())
                        .addValue("approve_date_time", aproveDateTime));
    }

    @Override
    public Optional<ApproveStatus> selectApproveStatus(String registrationId) {
        return namedParameterJdbcOperations
                .query("select approve_status from registrations where id = :id",
                        new MapSqlParameterSource("id", registrationId),
                        (rs, i) -> extractApproveStatus(rs))
                .stream()
                .findFirst();
    }

    @Override
    public List<RegistrationInfo<String>> selectRegistrations() {
        return namedParameterJdbcOperations
                .query("" +
                                " select id, registration_date_time, first_name, last_name, email, birthday, city_of_residence, city_of_registration, approve_status, approve_date_time" +
                                " from   registrations",
                        EmptySqlParameterSource.INSTANCE,
                        (rs, i) -> extractRegistrationInfo(rs));
    }

    private RegistrationInfo<String> extractRegistrationInfo(ResultSet rs) throws SQLException {
        return RegistrationInfo.of(
                rs.getObject("id", registrationIdClass),
                rs.getTimestamp("registration_date_time").toLocalDateTime(),
                extractUserInfo(rs),
                extractApproveStatus(rs),
                rs.getTimestamp("approve_date_time").toLocalDateTime()
        );
    }

    private UserInfo extractUserInfo(ResultSet rs) throws SQLException {
        return UserInfo.of(
                rs.getString("first_name"),
                rs.getString("last_name"),
                rs.getString("email"),
                rs.getDate("birthday").toLocalDate(),
                extractCity(rs, "city_of_residence"),
                extractCity(rs, "city_of_registration")
        );
    }

    private City extractCity(ResultSet rs,
                             String columnName) throws SQLException {
        return City.valueOf(rs.getString(columnName));
    }

    private ApproveStatus extractApproveStatus(ResultSet rs) throws SQLException {
        return ApproveStatus.valueOf(rs.getString("approve_status"));
    }

}
