package local.quest.api.db;

import local.quest.common.ApproveStatus;
import local.quest.common.model.RegistrationInfo;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface ApiServiceRepository<I> {

    void insertRegistrationInfo(RegistrationInfo<I> registrationInfo);

    void updateApproveStatus(I registrationId,
                             ApproveStatus approveStatus,
                             LocalDateTime aproveDateTime);

    Optional<ApproveStatus> selectApproveStatus(I registrationId);

    List<RegistrationInfo<I>> selectRegistrations();

}
