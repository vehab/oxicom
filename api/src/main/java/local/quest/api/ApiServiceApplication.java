package local.quest.api;

import local.quest.api.db.ApiServiceRepository;
import local.quest.api.service.ApiService;
import local.quest.common.ApproveStatus;
import local.quest.common.City;
import local.quest.common.config.JacksonConfig;
import local.quest.common.config.RabbitConsumerConfig;
import local.quest.common.config.RabbitProducerConfig;
import local.quest.common.config.RabbitQueueConfig;
import local.quest.common.model.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.function.client.WebClient;

import java.lang.invoke.MethodHandles;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Month;

@SpringBootApplication(
        scanBasePackages = "local.quest.api",
        scanBasePackageClasses = {
                JacksonConfig.class,
                RabbitQueueConfig.class, RabbitConsumerConfig.class, RabbitProducerConfig.class})
public class ApiServiceApplication {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(ApiServiceApplication.class);
        app.setWebApplicationType(WebApplicationType.REACTIVE);
        app.run(args);
    }

    @Bean
    public WebClient webClientForIdGeneratorService() {
        return WebClient.builder()
                .baseUrl("http://localhost:8081/id-gen/v1")
                .build();
    }

    @Bean
    public CommandLineRunner init(ApiService<String> apiService,
                                  ApiServiceRepository<String> apiServiceRepository) {
        return args -> {
            UserInfo userInfo =
                    UserInfo.of("Mark", "Twain", "mark-twain@writer.book",
                            LocalDate.of(1835, Month.NOVEMBER, 30),
                            City.anapa, City.berlin);

            apiService.sendVerification(userInfo)
                    .doOnNext(registrationId -> printRegistrationInfo(apiService, registrationId))
                    //.doOnNext(registrationId -> apiServiceRepository.updateApproveStatus(registrationId, ApproveStatus.approved, LocalDateTime.now()))
                    .delayElement(Duration.ofMillis(5_000))
                    .doOnNext(registrationId -> printRegistrationInfo(apiService, registrationId))
                    .subscribe(registrationId -> { /**/ }, thr -> log.error("init error", thr), () -> log.info("init completed"));
        };
    }

    private void printRegistrationInfo(ApiService<String> apiService,
                                       String registrationId) {
        ApproveStatus approveStatus = apiService.checkVerificationStatus(registrationId);
        log.warn("init : registrationId='{}', approveStatus={}", registrationId, approveStatus);
    }

}
